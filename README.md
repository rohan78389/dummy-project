# README #

### What is this repository for? ###

* Dummy project - Coupon management system.

### How do I get set up? ###

* Create a .env file from env.example file. 
* Create an new database and edit database connection details in .env file accordingly
* Run commands
* composer install
* composer dump:autoload [To generate optimized autoload files]
* php artisan key:generate
* php artisan:migrate [This will create all required tables]
* php artisan server [to start using the project]

### File locations ###

* Request validation login [common for add and edit requests] is in 
*   app\Http\Requests\StoreCoupon.php
* 
* Logic for sending email coupon expiry alert email is written in 
*   app\Mail\ExpiringCoupons.php





