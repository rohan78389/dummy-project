<?php

use Illuminate\Http\Request;
use \App\Coupon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| 200: OK. The standard success code and default option.
| 201: Object created. Useful for the store actions.
| 204: No content. When an action was executed successfully, but there is no content to return.
|
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('coupons', 'CouponController@index');
Route::get('coupons/{coupon}', 'CouponController@show')->name("get_coupon");
Route::post('coupons', 'CouponController@store')->name("addCouponApi");
Route::put('coupons/{coupon}', 'CouponController@update')->name("updateCouponApi");
Route::delete('coupons/{coupon}', 'CouponController@delete')->name("delete_coupon");

