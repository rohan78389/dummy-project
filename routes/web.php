<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

\Illuminate\Session\Middleware\StartSession::class;

Route::get('/', 'CouponController@couponList')->name("index");
Route::get('coupons', 'CouponController@couponList')->name("coupon_list");
Route::get('coupons/add', 'CouponController@createCoupon')->name("create_coupon");
Route::get('coupons/getAll', 'CouponController@sendCouponExpiryAlert');
Route::get('coupons/{coupon}', 'CouponController@couponDetails')->name("coupon_details");

Route::get('coupons/edit/{coupon}', 'CouponController@editCoupon')->name("edit_coupon");




