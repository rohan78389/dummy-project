<?php

use Illuminate\Database\Seeder;
use App\Coupon;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coupon::truncate();
        $faker = \Faker\Factory::create();
        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Coupon::create([
                'name' => $faker->sentence,
                'description' => $faker->paragraph,
                "valid_from" => $faker->dateTime(),
                "valid_upto" => $faker->dateTimeAd($max="30 days"),
                "coupon_value" => $faker->randomFloat($nbmaxDecimals = 2,$min=0,$max=200),
                "quantity" => $faker->numberBetween($min=10,$max=100),
                "limit_per_user" => 10,
                "is_active" => 1
            ]);
        }
    }
}
