<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = ["name","description","valid_from","valid_upto","coupon_value","quantity","limit_per_user","is_active"];
}
