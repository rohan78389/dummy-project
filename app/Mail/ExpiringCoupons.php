<?php

namespace App\Mail;

use App\Coupon;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExpiringCoupons extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $coupons = Coupon::all()->toArray();

        $expiring_coupons = array();

        $i = 0;
        foreach ($coupons as $coupon)
        {
            $expiry_date = $coupon['valid_upto'];
            $end = Carbon::parse($expiry_date);
            $now = Carbon::now();
            $dayDiff = $end->diffInDays($now);

            if($dayDiff < 3)
            {
                $expiring_coupons[$i] = $coupon;
                $i++;
            }

        }

        $response = array();
        $response['coupons'] = $expiring_coupons;

        return $this->view('mail.expiringCouponAlert')->with($response);
    }
}
