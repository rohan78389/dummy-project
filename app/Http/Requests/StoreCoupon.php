<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCoupon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'valid_from' => 'required|date|after:yesterday',
            'valid_upto' => 'required|date|after:valid_from',
            'coupon_value' => 'required|numeric|min:0',
            'quantity' => 'required|integer|min:0',
            'limit_per_user' => 'integer|nullable'
        ];
    }


}
