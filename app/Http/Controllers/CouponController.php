<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Http\Requests\StoreCoupon;
use App\Mail\ExpiringCoupons;
use Carbon\Carbon;
use Illuminate\Http\Request;


class CouponController extends Controller
{
    public function index()
    {
        return Coupon::all();
    }

    public function show(Coupon $coupon)
    {
        return $coupon;
    }

    public function store(StoreCoupon $request)
    {
        $data = Coupon::create($request->all());

        $request->session()->flash('message', 'New coupon added successfully.');
        $request->session()->flash('message-type', 'success');

        return $data;
    }

    public function update(StoreCoupon $request, Coupon $coupon)
    {
        $coupon->update($request->all());

        $request->session()->flash('message', 'Coupon updated successfully.');
        $request->session()->flash('message-type', 'success');

        return response()->json($coupon, 200);
    }

    public function delete(Coupon $coupon,Request $request)
    {
        if($coupon->delete())
        {
            $request->session()->flash('message', 'Coupon deleted successfully.');
            $request->session()->flash('message-type', 'danger');

            return "true";
        }
    }


    public function couponList()
    {
        $response = array();
        $coupons = Coupon::all()->toArray();

        $response['coupons'] = $coupons;

        return view('coupons.list', $response);
    }

    public function createCoupon()
    {
        return view('coupons.fillable');
    }

    public function couponDetails(Coupon $coupon)
    {
        $response = array();
        $response['coupon'] = $coupon->toArray();

        return view('coupons.details',$response);
    }

    public function editCoupon(Coupon $coupon)
    {
        $response = array();
        $coupon = $coupon->toArray();
        $response['coupon'] = $coupon;
        $response['id'] = $coupon['id'];

        return view('coupons.fillable',$response);
    }

    public function sendCouponExpiryAlert()
    {
        Mail::to("admin@dummyproject.abc")->send(new ExpiringCoupons());
    }
}
