@extends('layouts.app')

@section("title",$coupon['name'])

@section('content')

    <div class="col-xs-12">
        <h2>Coupon Details</h2>

        @if(Session::has("message"))
            <p style="margin-top: 10px;" class="col-xs-12 alert alert-{{ Session::get("message-type") }}">{{ Session::get("message") }}</p>
        @endif

        <hr>

        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="col-md-3">
                        <strong>Title</strong>
                    </td>
                    <td>
                        {{ $coupon['name'] }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Description</strong>
                    </td>
                    <td>
                        {!! $coupon['description'] !!}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Valid from</strong>
                    </td>
                    <td>
                        @datetime($coupon['valid_from'])
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Valid upto</strong>
                    </td>
                    <td>
                        @datetime($coupon['valid_upto'])
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Coupon value</strong> (INR)
                    </td>
                    <td>
                        {{ $coupon['coupon_value'] }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Total coupons</strong>
                    </td>
                    <td>
                        {{ $coupon['quantity'] }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Max redeem per user</strong>
                    </td>
                    <td>
                        {{ $coupon['limit_per_user'] }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Status</strong>
                    </td>
                    <td>
                        @if($coupon['is_active'])
                            <span class="text-success">Active</span>
                            @else
                            <span class="text-warning">Paused</span>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>

        <hr>
        <a href="{{ route("coupon_list") }}" class="btn btn-default">Back</a>

        <a href="{{ route("edit_coupon",[$coupon['id']]) }}" class="pull-right btn btn-warning" >Edit</a>
        <button data-url="{{ route("delete_coupon",[$coupon['id']]) }}" class="pull-right delete btn btn-danger" style="margin-right:10px;">Delete</button>


    </div>



    @include("components.deleteAlert")


@endsection