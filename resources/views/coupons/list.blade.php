@extends('layouts.app')

@section("title","All coupons")

@section('content')

    <div class="col-xs-12">
        <h2>All Coupons
            <a href="{{ route("create_coupon") }}" class="btn btn-success pull-right">
                <span style="top:-1px;" class="glyphicon glyphicon-plus"></span> New coupon
            </a>
        </h2>

        @if(Session::has("message"))
            <p style="margin-top: 10px;" class="col-xs-12 alert alert-{{ Session::get("message-type") }}">{{ Session::get("message") }}</p>
        @endif

        <table class="table table-responsive">
            <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th class="col-md-2">
                        Coupon
                    </th>
                    <th class="col-md-3">
                        Description
                    </th>
                    <th>
                        Expiring on
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Actions
                    </th>
                </tr>
            </thead>
            <tbody>
                @isset($coupons)
                    @foreach($coupons as $coupon)
                        @php
                            $end = \Carbon\Carbon::parse($coupon['valid_upto']);
                            $now = \Carbon\Carbon::now();
                            $length = $end->diffInDays($now);
                        @endphp
                        <tr class="@if($length < 3) danger @endif">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $coupon['name'] }}</td>
                            <td>{{ str_limit($coupon['description'],100,"..") }}</td>
                            <td>@datetime($coupon['valid_upto'])
                                @if($length < 3)
                                    <br>
                                    <span class="label label-danger">Expiring in {{ $length }} @if($length>1) days @else day @endif</span>
                                @endif
                            </td>
                            <td>{{ $coupon['quantity'] }}</td>
                            <td>
                                <div class="bs-example">
                                    <a href="{{ route("coupon_details",[$coupon['id']]) }}" data-id="{{ $coupon['id'] }}" class="details btn btn-info">Details</a>
                                    <a href="{{ route("edit_coupon",[$coupon['id']]) }}" class="btn btn-warning">Edit</a>
                                    <button data-url="{{ route("delete_coupon",[$coupon['id']]) }}" class="delete btn btn-danger">Delete</button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endisset
            </tbody>
        </table>
    </div>



    @include("components.deleteAlert")


@endsection