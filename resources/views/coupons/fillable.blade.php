@extends('layouts.app')

@php
    if(isset($id))
    {
        $title = "Edit coupon";
        $post_type = "PUT";
        $submit_url = route("updateCouponApi",[$id]);
    }
    else
    {
        $title = "Create new coupon";
        $post_type = "POST";
        $submit_url = route("addCouponApi");
    }
@endphp

@section("title",$title)

@section("content")
    <div class="col-xs-12">
        <h2>
            {{ $title }}
        </h2>

        <div class="col-xs-12 well">
        <form>
            <div class="form-group">
                <label for="name">Coupon title</label>
                <input type="text" name="name" class="form-control" id="name" placeholder=""
                       value="@isset($coupon['name']){{ (old('name',$coupon['name'])) }}@else{{ (old('name')) }}@endisset">
                <span class="text-danger name-error"></span>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description">@isset($coupon['description']){{ (old('description',$coupon['description'])) }}@else{{ (old('description')) }}@endisset</textarea>
                <span class="text-danger description-error"></span>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label for="valid_from">Valid from</label>
                        <input type="text" name="valid_from" 
                               value="@isset($coupon['valid_from']){{ (old('valid_from',$coupon['valid_from'])) }}@else{{ (old('valid_from')) }}@endisset"
                               class="datepicker form-control" id="valid_from" placeholder="">
                        <span class="text-danger valid_from-error"></span>
                    </div>
                </div>
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label for="expiry">Expiry date</label>
                        <input type="text" name="valid_upto" value="@isset($coupon['valid_upto']){{ (old('valid_upto',$coupon['valid_upto'])) }}@else{{ (old('valid_upto')) }}@endisset" class="datepicker form-control" id="expiry" placeholder="">
                        <span class="text-danger valid_upto-error"></span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label for="value">Coupon value (INR)</label>
                        <input type="number" name="coupon_value" value="@isset($coupon['coupon_value']){{ (old('coupon_value',$coupon['coupon_value'])) }}@else{{ (old('coupon_value')) }}@endisset" class="form-control" id="value" placeholder="">
                        <span class="text-danger coupon_value-error"></span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label for="qty">Quantity</label>
                        <input type="number" name="quantity" value="@isset($coupon['quantity']){{ (old('quantity',$coupon['quantity'])) }}@else{{ (old('quantity')) }}@endisset" class="form-control" id="qty" placeholder="">
                        <span class="text-danger quantity-error"></span>
                    </div>
                </div>
                <div class="col-xs-6 col-md-4">
                    <div class="form-group">
                        <label for="max_redeem">Max redeem per user</label>
                        <input type="number" name="limit_per_user" value="@isset($coupon['limit_per_user']){{ (old('limit_per_user',$coupon['limit_per_user'])) }}@else{{ (old('limit_per_user')) }}@endisset" class="form-control" id="max_redeem" placeholder="">
                        <span class="text-danger limit_per_user-error"></span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="checkbox">
                <label>
                    <input @if(isset($coupon['is_active']) == 1) checked @endif name="is_active" value="1" type="checkbox"> Is active
                </label>
            </div>
            <hr>
            <a id="saveForm" class="btn btn-success">Save</a>
            <a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
        </form>
        </div>
    </div>

@endsection

@push("scripts")
    <script>

        $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd',minDate:0 });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        function getFormData($form){
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};

            $.map(unindexed_array, function(n, i){
                indexed_array[n['name']] = n['value'];
            });

            return indexed_array;
        }

        $(function(){
            $("#saveForm").click(function() {

                $("span.text-danger").html("");

                var user_limit = $("input[name=limit_per_user]").val();
                if (user_limit != "")
                {
                    var quantity = $("input[name=quantity]").val();
                    if(user_limit > quantity)
                    {
                        $('.limit_per_user-error').html('Max redeem per user should be less than total number of coupons.');
                        return false;
                    }
                }

                var $form = $("form");
                var data = getFormData($form);

                var submit_url = '{{$submit_url}}';
                var post_type = '{{ $post_type }}';

                $.ajax({
                    url: submit_url,
                    type: post_type,
                    data: data,
                    success: function(result) {
                        location.href = '{{ route("coupon_list") }}';
                    },
                    error: function( json )
                    {
                        if(json.status === 422) {
                            var errors = json.responseJSON;
                            console.log(errors);
                            $.each(errors.errors, function (key, value) {
                                $('.'+key+'-error').html(value);
                            });
                        }
                    }
                });

            });
        });
    </script>
@endpush