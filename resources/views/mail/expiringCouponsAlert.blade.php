<p>
    Admin,<br>
    <br>
    Below is the list of coupons expiring in less than 3 days:<br>
    <ul>
    @foreach($coupons as $coupon)
        <li>
            <a href="{{ route("coupon_details",[$coupon['id']]) }}">{{ $coupon['name'] }}</a>
        </li>
    @endforeach
    </ul>
</p>