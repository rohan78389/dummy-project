<div id="confirm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete coupon</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure?</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="delete">Delete</button>
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@push("scripts")
    <script>
        $('button.delete').on('click', function(e) {
            var this_url = $(this).attr("data-url");
            e.preventDefault();
            $('#confirm').modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '#delete', function(e) {
                    $.ajax({
                        url: this_url,
                        type: "DELETE",
                        success: function(result) {
                            location.href = '{{ route("coupon_list") }}';
                        },
                        error: function( json )
                        {
                            if(json.status === 422) {
                                var errors = json.responseJSON;
                            }
                        }
                    });
                });
        });
    </script>
@endpush